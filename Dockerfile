FROM alpine:3.7
LABEL maintainer="Sebastian Hugentobler <sebastian@vanwa.ch>"

ENV S6_OVERLAY_VERSION=v1.21.4.0

RUN apk --no-cache add libressl syslog-ng incron && \
    wget -qO- https://github.com/just-containers/s6-overlay/releases/download/$S6_OVERLAY_VERSION/s6-overlay-amd64.tar.gz | tar -xz -C / && \
    apk del libressl

RUN apk --no-cache upgrade

ADD /rootfs /

ENTRYPOINT ["/init"]

